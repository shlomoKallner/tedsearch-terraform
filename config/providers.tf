provider "aws" {
  profile    = "default"
  region     = "us-east-2"
  version = "~> 2.45"
}
