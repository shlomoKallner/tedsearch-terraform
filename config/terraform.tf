terraform {
  backend "s3" {
    bucket = "space.shlomosweb.portfolio.devops-course.terraform"
    key    = "tfstate/terraform-aws.tfstate"
    region = "us-east-2"
  }
}
